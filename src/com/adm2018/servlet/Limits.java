package com.adm2018.servlet;

import com.adm2018.dbInterface.DbManager;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(name = "Limits", urlPatterns = {"/limits"})
public class Limits extends HttpServlet {

    static final int MAX_BRIGHTNESS = 120;
    static final int MIN_BRIGHTNESS = 2;
    static final int MIN_TEMPERATURE = 18;
    static final int MAX_TEMPERATURE = 25;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //In this servlet the only parameter is passed in cmd, and can be temperature or brightness
        String command = request.getParameter("cmd");
        if (command != null)
        {
            List<String> list1 = new ArrayList<>();
            List<String> list2 = new ArrayList<>();
            List<String> list3 = new ArrayList<>();
            List<String> list4 = new ArrayList<>();
            DbManager dbManager = new DbManager();
            PrintWriter writer = response.getWriter();
            //decide which info the user is asking
            switch (command) {
                case "brightness":
                    list1.addAll(toJsonFormat(dbManager.getOverLimitBrightness("room1", MAX_BRIGHTNESS-2)));
                    list2.addAll(toJsonFormat(dbManager.getUnderLimitBrightness("room1", MIN_BRIGHTNESS+2)));
                    list3.addAll(toJsonFormat(dbManager.getOverLimitBrightness("room2", MAX_BRIGHTNESS-2)));
                    list4.addAll(toJsonFormat(dbManager.getUnderLimitBrightness("room2", MIN_BRIGHTNESS+2)));
                    break;
                case "temperature":
                    list1.addAll(toJsonFormat(dbManager.getOverLimitTemperature("room1", MAX_TEMPERATURE-2)));
                    list2.addAll(toJsonFormat(dbManager.getUnderLimitTemperature("room1", MIN_TEMPERATURE+2)));
                    list3.addAll(toJsonFormat(dbManager.getOverLimitTemperature("room2", MAX_TEMPERATURE-2)));
                    list4.addAll(toJsonFormat(dbManager.getUnderLimitTemperature("room2", MIN_TEMPERATURE+2)));
                    break;
            }
            //we insert all the 4 lists in separate properties of a json object, along with the legal boundaries
            writer.print("{\"over1\":"+list1+", \"under1\":"+list2+", \"over2\":"+list3+", \"under2\":"+list4+
                    ", \"max_temperature\":"+MAX_TEMPERATURE+",\"min_temperature\":"+MIN_TEMPERATURE+
                    ", \"max_brightness\":"+MAX_BRIGHTNESS+",\"min_brightness\":"+MIN_BRIGHTNESS+"}");
            writer.close();
            writer.flush();

        }

        response.setContentType("application/json");
        //Set already expired content to avoid caching
        response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
        response.setHeader("Cache-Control", "no-store, no-cache, "
                + "must-revalidate");
    }

    protected List<String> toJsonFormat(List<Document> documents){
        //parallel conversion in json
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        documents.parallelStream().forEachOrdered(d -> list.add(d.toJson()));
        return list;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}

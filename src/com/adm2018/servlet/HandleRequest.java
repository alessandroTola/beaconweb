package com.adm2018.servlet;

import com.adm2018.dbInterface.DbManager;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebServlet(name = "HandleRequest", urlPatterns = {"/handlerequest"})
public class HandleRequest extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //In this servlet we should receive in cmd the information desired and possibly the timestamp in time
        String command = request.getParameter("cmd");
        String startString = request.getParameter("time");
        Long startTime;
        Long endTime = 0L;
        Boolean timed = false;
        startTime = Long.parseLong(startString);
        if (startTime != -1){ //the start time is set to -1 if the time is not selected by the user
            timed = true;
            endTime  = startTime + 86400000; //sum the number of millisecond in a day
        }
        if (command != null)
        {
            // Decide which information the user is asking
            DbManager dbManager = new DbManager();
            PrintWriter writer = response.getWriter();
            switch (command) {
                case "room1":
                case "room2":
                    List<String> list;
                    if (timed) {
                        list = toJsonFormat(dbManager.getDataFromRoom(command, startTime, endTime));
                    } else {
                        list = toJsonFormat(dbManager.getDataFromRoom(command));
                    }
                    writer.print(list);
                    writer.close();
                    writer.flush();
                    break;
                case "people":
                    List<String> peopleList;
                    if (timed) {
                        peopleList = toJsonFormat(dbManager.getPeople(startTime, endTime));
                    } else {
                        peopleList = toJsonFormat(dbManager.getPeople());
                    }

                    writer.print(peopleList);
                    writer.close();
                    writer.flush();
                    break;
                case "temperature":
                    List<String> b1;
                    List<String> b2;
                    if (timed) {
                        b1 = toJsonFormat(dbManager.getTemperature("room1", startTime, endTime));
                        b2 = toJsonFormat(dbManager.getTemperature("room2", startTime, endTime));
                    } else {
                        b1 = toJsonFormat(dbManager.getTemperature("room1"));
                        b2 = toJsonFormat(dbManager.getTemperature("room2"));
                    }
                    //we write the 2 list separately, as properties of a json object
                    writer.print("{ \"room1\": ");
                    writer.print(b1);
                    writer.print(", \"room2\" : ");
                    writer.print(b2);
                    writer.print("}");
                    writer.close();
                    writer.flush();
                    break;
                case "brightness":
                    List<String> t1;
                    List<String> t2;
                    if (timed) {
                        t1 = toJsonFormat(dbManager.getBrightness("room1", startTime, endTime));
                        t2 = toJsonFormat(dbManager.getBrightness("room2", startTime, endTime));
                    } else {
                        t1 = toJsonFormat(dbManager.getBrightness("room1"));
                        t2 = toJsonFormat(dbManager.getBrightness("room2"));
                    }
                    //we write the 2 list separately, as properties of a json object
                    writer.print("{ \"room1\": ");
                    writer.print(t1);
                    writer.print(", \"room2\" : ");
                    writer.print(t2);
                    writer.print("}");
                    writer.close();
                    writer.flush();
                    break;
            }
            response.setContentType("application/json");
            //Set already expired content to avoid caching
            response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
            response.setHeader("Cache-Control", "no-store, no-cache, "
                    + "must-revalidate");
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected List<String> toJsonFormat(List<Document> documents){
        //parallel conversion in json
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        documents.parallelStream().forEachOrdered(d -> list.add(d.toJson()));
        return list;
    }
}

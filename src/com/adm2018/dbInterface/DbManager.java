package com.adm2018.dbInterface;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Projections.computed;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static java.util.Arrays.asList;

public class DbManager {
    /**
     *
     * @return The initialized database
     */
    private MongoDatabase getDatabase(){
        //MongoClientURI uri = new MongoClientURI("mongodb://FiMaAl:Adm2018Bello@ds119268.mlab.com:19268/officebeacon");
        MongoClientURI uri = new MongoClientURI("mongodb://localhost");
        MongoClient mongoClient = new MongoClient(uri);
        return mongoClient.getDatabase("officebeacon");
    }

    /**
     *
     * @param collection The name of the collection
     * @return  List of Document with all collection's temperature and timestamp
     */
    public List<Document> getTemperature(String collection){
        String query = "";
        List<String> fields = new ArrayList<>();
        fields.add("temperature");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @param start Start interval of time
     * @param end End interval of time
     * @return List of Document with all collection's temperature and timestamp
     */
    public List<Document> getTemperature(String collection, Long start, Long end){
        String query = " { $and: [ { timestamp: { $gt: " + start.toString() + " } }, " +
                                  "{ timestamp: { $lt: " + end.toString() + "} } ]}";
        List<String> fields = new ArrayList<>();
        fields.add("temperature");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @return List of Document with all collection's ambientLight and timestamp
     */
    public List<Document> getBrightness(String collection) {
        String query = "";
        List<String> fields = new ArrayList<>();
        fields.add("ambientLight");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @param start Start interval of time
     * @param end End interval of time
     * @return List of Document with all collection's ambientLight and timestamp
     */
    public List<Document> getBrightness (String collection, Long start, Long end){
        String query = "{ $and: [ { timestamp: { $gt: " + start.toString() + " } }, " +
                                 "{ timestamp: { $lt: " + end.toString() + "} } ] }";
        List<String> fields = new ArrayList<>();
        fields.add("ambientLight");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @return List of Document with timestamp of the door collection
     */
    public List<Document> getPeople() {
        String query = "";
        List<String> fields = new ArrayList<>();

        return execQueryFind("door", query, fields);
    }

    /**
     *
     * @param start Start interval of time
     * @param end End interval of time
     * @return List of Document with all collection's count and timestamp inside the lime interval
     */
    public List<Document> getPeople(Long start, Long end) {
        String query = "{ $and: [ { timestamp: { $gt: " + start.toString() + " } }, " +
                                 "{ timestamp: { $lt: " + end.toString() + "} } ] }";
        List<String> fields = new ArrayList<>();

        return execQueryFind("door", query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @return  List of Document with all collection's temperature, ambientLight and timestamp
     */
    public List<Document> getDataFromRoom(String collection){
        String query = "";
        List<String> fields = new ArrayList<>();
        fields.add("temperature");
        fields.add("ambientLight");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @param start Start interval of time
     * @param end End interval of time
     * @return List of Document with all collection's temperature, ambientLight and timestamp inside the lime interval
     */
    public List<Document> getDataFromRoom(String collection, Long start, Long end){
        String query = " { $and: [ { timestamp: { $gt: " + start.toString() + " } }, " +
                                  "{ timestamp: { $lt: " + end.toString() + "} } ] }";
        List<String> fields = new ArrayList<>();
        fields.add("temperature");
        fields.add("ambientLight");
        return execQueryFind(collection, query, fields);
    }

    /**
     *
     * @param collection The name of the collection
     * @param limit Value of brightness limit
     * @return  List of Document with collection's brightness over the limit
     */
    public List<Document> getOverLimitBrightness(String collection, int limit){
        return execQueryAggregate(collection, "ambientLight", limit, 0);
    }


    /**
     *
     * @param collection The name of the collection
     * @param limit Value of temperature limit
     * @return List of Document with collection's temperature over the limit
     */
    public List<Document> getOverLimitTemperature(String collection, int limit){
        return execQueryAggregate(collection, "temperature", limit, 0);
    }

    /**
     *
     * @param collection The name of the collection
     * @param limit Value of brightness limit
     * @return  List of Document with collection's brightness under the limit
     */
    public List<Document> getUnderLimitBrightness(String collection, int limit){
        return execQueryAggregate(collection, "ambientLight", limit, 1);
    }

    /**
     *
     * @param collection The name of the collection
     * @param limit Value of temperature limit
     * @return List of Document with collection's temperature under the limit
     */
    public List<Document> getUnderLimitTemperature(String collection, int limit){
        return execQueryAggregate(collection, "temperature", limit, 1);
    }

    /**
     *
     * @param collection The name of the collection
     * @param queryString Query in string format
     * @param fields The queary's output fields
     * @return List of resultant Documents from the query
     */
    private List<Document> execQueryFind(String collection, String queryString, List<String> fields) {

        MongoDatabase database = getDatabase();
        MongoCollection<Document> coll = database.getCollection(collection);
        List<Document> myDocs;
        fields.add("timestamp");
        if("".equals(queryString)) {
            myDocs = coll.find().projection(include(fields)).into(new ArrayList<>());
        } else {
            Document doc = Document.parse(queryString);
            myDocs = coll.find(doc).projection(include(fields)).into(new ArrayList<>());
        }
        return myDocs;
    }

    /**
     *
     * @param collection The name of the collection
     * @param field The queary's output fields
     * @param limit The value limit for the query, max or min temperature or brightness
     * @param type If the query in greater or less then
     * @return List of resultant Documents from the query
     */
    private List<Document> execQueryAggregate(String collection, String field, int limit, int type) {

        MongoDatabase database = getDatabase();
        MongoCollection<Document> coll = database.getCollection(collection);
        List<Document> myDocs;

        //Epoch time
        int oneDay = 86400000;
        int starTimeNine = oneDay / 24 * 9;
        int endTimeEighteen = oneDay / 24 * 18;
        Bson beacon;
        Bson gtORlt;
        BasicDBList modArgs = new BasicDBList();
        modArgs.add("$timestamp");
        modArgs.add(oneDay);


        if (type == 0) gtORlt = Filters.gt(field, limit);
        else {
            gtORlt = Filters.lt(field,limit);
        }

        if("room2".equals(collection)) {
            beacon = Filters.eq("id","[0a2f3fb363addf53e2a52156a6b2c82c]");
        } else {
            beacon = Filters.eq("id","[56f8732d98f7efc1df6a8f71013db03b]");
        }

        myDocs = coll.aggregate(asList(match(beacon),
                                       project(fields(include(field, "timestamp"),
                                               computed("time", new BasicDBObject("$mod", modArgs)))),
                                       match(Filters.and(Filters.gt("time", starTimeNine),
                                                         Filters.lt("time", endTimeEighteen))),
                                       match(gtORlt)
                                )).into(new ArrayList<>());
        return myDocs;
    }
}

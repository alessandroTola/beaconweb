<%--
  Created by IntelliJ IDEA.
  User: martina
  Date: 31/01/18
  Time: 16.25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <title>ADM2018 - Home</title>
      <link rel="shortcut icon" href="img/beacon_icon.png">
      <script src="js/http_d3js.org_d3.v4.js"></script>
      <script src="js/jquery-3.3.1.min.js"></script>
      <script src="js/chart-2.7.1.min.js"></script>
      <script src="js/home.js"></script>
      <link href="beacon_style.css" rel="stylesheet" type="text/css">
  </head>

  <body>

    <div id="buttons_div1">

        <button class="cmd" id="room1_btn" value="room1">Room 1</button>
        <button class="cmd" id="room2_btn" value="room2">Room 2</button>
        <button class="cmd" id="temp_btn" value="temperature">Temperature</button>
        <button class="cmd" id="bright_btn" value="brightness">Brightness</button>
        <button class="cmd" id="people_btn" value="people">People</button>


        <p>Filter results by date:</p>
        <input id="time" type="date" min="2018-01-31" max="2018-02-06">
        <button class="filter" id="filter_btn">Filter</button>
        <button class="filter" id="reset_btn">Reset</button>

        <button id="limits" value="limits" onclick="window.location.href='limits.jsp'">Limits</button>

    </div>

    <canvas id="fst_chart"></canvas>
    <canvas id="snd_chart"></canvas>


  </body>
</html>

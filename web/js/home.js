var label_br = 'Brightness';
var label_br1 = 'Room1 Brightness';
var label_br2 = 'Room2 Brightness';
var label_tm = 'Temperature';
var label_tm1 = 'Room1 Temperature';
var label_tm2 = 'Room2 Temperature';
var label_pp = 'People';
var color_1 = 'rgb(255, 140, 26)';
var color_2 = 'teal';
var color_3 = 'red';
var current_cmd = '';

$(document).ready(function() {

    //gestione scelta dati
    $(".cmd").click(function () {
        changeColorSelectedButton(this, ".cmd");
        var cmd = $(this).attr('value');
        current_cmd = cmd;
        request(cmd, getTime());
    });

    //comando di filtro
    $("#filter_btn").click(function(){
        changeColorSelectedButton(this, ".filter");
        if(current_cmd.length > 0)
            request(current_cmd, getTime());
    });

    //reset filtro
    $("#reset_btn").click(function(){
        changeColorSelectedButton(this, ".filter");
        $("#time").val("");
        //request(current_cmd, getTime());
    });

    //caricamento automatico prima opzione
    //$('#room1_btn').trigger('click');
});

//richiesta get a handlerequest, filtro parametri di ritorno, disegno grafici
var request = function(cmd, time){

    console.log(cmd, time);
    resetCanvas();
    var canvas1 = $("#fst_chart");
    var canvas2 = $("#snd_chart");

    $.get({
        url: "handlerequest",
        data: {
            cmd : cmd,
            time: time
        },
        success: function (response) {
            response = JSON.parse(response);

            switch(cmd){
                case "room1":
                case "room2":
                    var data_light = [];
                    var data_temp = [];
                    getLightandTemperature(response, data_light, data_temp);
                    drawChart(canvas1, [new Dataset(label_br, color_1, data_light)]);
                    drawChart(canvas2, [new Dataset(label_tm, color_2, data_temp)]);
                    break;
                case "temperature":
                    var data_temp1 = getSingleParameter(response['room1'], 'temperature');
                    var data_temp2 = getSingleParameter(response['room2'], 'temperature');
                    drawChart(canvas1, [
                        new Dataset(label_tm1, color_1, data_temp1),
                        new Dataset(label_tm2, color_2, data_temp2)
                    ]);
                    break;
                case "brightness":
                    var data_light1 = getSingleParameter(response['room1'], 'ambientLight');
                    var data_light2 = getSingleParameter(response['room2'], 'ambientLight');
                    drawChart(canvas1, [
                        new Dataset(label_br1, color_1, data_light1),
                        new Dataset(label_br2, color_2, data_light2)
                    ]);
                    break;
                case "people":
                    var timestamps = getTimestamp(response);
                    var labels = [];
                    var values = [];
                    dataFrequencyPerInterval(timestamps, 10800000, labels, values);
                    var dataset = new Dataset(label_pp, color_3, values);
                    dataset.backgroundColor = color_3;
                    drawBarChart(canvas1, labels, [dataset]);
                    break;
            }
        }
    })
};

//restituisce il timestamp del filtro (se non è stato impostato il filtro restituisce -1)
var getTime = function(){
    var time = $("#time").val();

    if (time){
        return (new Date(time)).getTime();
    } else
        return -1;
};

//restituisce la lista dei timestamp di un elenco di oggetti
var getTimestamp = function(data){
    var list = [];
    data.forEach(function(element){
        var time = element['timestamp'];
        list.push(parseInt(time['$numberLong']));
    });
    return list;
};

//calcola la frequenza di pacchetti suddivisi per intervalli (intervallo di 3 ore hardcoded)
var dataFrequencyPerInterval = function(data, interval, labels, values){
    var sum = 0;
    var current = data[0] + interval;


    data.forEach(function(element){
        while (element > current){
            var date1 = new Date(current - interval);
            var date2 = new Date(current);
            labels.push(date1.toDateString() + " - " + date1.getHours() + " - " + date2.getHours());
            //labels.push(date1.getHours() + "-" + date2.getHours());
            values.push(sum);
            current += interval;
            sum = 0;
        }
        sum += 1;
    });

    if(sum > 0){
        var date1 = new Date(current - interval);
        var date2 = new Date(current);
        labels.push(date1.toDateString() + " - " + date1.getHours() + " - " + date2.getHours());
        //labels.push(date1.getHours() + "-" + date2.getHours());
        values.push(sum);
    };
};

//costruisce una lista di oggetti del tipo  {x: timestamp e y: valore monitorato}
var getSingleParameter = function(data, parameter){
    var paramList = [];
    data.forEach(function(element){
        var time = element['timestamp'];
        paramList.push({x: parseInt(time['$numberLong']), y: element[parameter]});
    });
    return paramList;
};

//popola due liste di oggetti del tipo {x : timestamp, y: valore monitorato}, contenenti una i valori relativi alla
//luminosità e una i valori della temperatura
var getLightandTemperature = function(data, param_light, param_temp){
    data.forEach(function(element){
        var time = element['timestamp'];
        param_light.push({x: parseInt(time['$numberLong']), y: element['ambientLight']});
        param_temp.push({x: parseInt(time['$numberLong']), y: element['temperature']});
    });
};

//inizializza il Dataset per il disegno del grafico
function Dataset(label, borderColor, data){
    this.label = label;
    this.borderColor = borderColor;
    this.data = data;
};

//disegna un grafico data un oggetto di tipo canvas e uno o più datasets
var drawChart = function(canvas, datasets){
    var chart = new Chart(canvas, {
        type: 'line',
        data: {
            datasets: datasets
        },
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    type: 'linear',
                    position: 'bottom',
                }],
                yAxes: [{
                    display: true,
                    type: 'linear',
                    stepSize: 5
                }]
            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            animation: {
                duration: 0 // general animation time
            },
            hover: {
                animationDuration: 0 // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0, // animation duration after a resize
            responsive : false
        }
    });
};

//disegna un grafico a barre (per l'istogramma delle persone)
var drawBarChart = function(canvas, labels, datasets){
    var chart = new Chart(canvas, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: datasets
        },
        options: {
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            animation: {
                duration: 0 // general animation time
            },
            hover: {
                animationDuration: 0 // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0, // animation duration after a resize
            responsive : false,
            maxBarThickness: 10
        }
    });
};

//resetta le canvas
var resetCanvas = function(){
    $('#fst_chart').remove();
    $('#snd_chart').remove();
    $('body').append('<canvas id="fst_chart"><canvas>');
    $('body').append('<canvas id="snd_chart"><canvas>');
}

//modifica il colore del bottone selezionato
var changeColorSelectedButton = function(btn, btn_class){
    $(btn_class).css({
        "background-color": "white",
        "color": "black"
    });

    $(btn).css({
        "background-color": "teal",
        "color": "white"
    })
}


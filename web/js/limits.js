$(document).ready(function(){

    //click bottoni
    $("button").click(function(){
        changeColorSelectedButton(this, ".cmd")
        var cmd = $(this).attr('value');
        request(cmd);
    });

    //caricamento automatico prima opzione
    //$("#temp_log_btn").trigger('click');

});

//richiesta alla servlet
var request = function(cmd){

    $.get({
        url: "limits",
        data: { cmd: cmd },
        success: function(response){
            //reset tabella
            $("table").remove();
            var data = JSON.parse(response);
            var parameter, over, under;

            //console.log(data);
            switch(cmd){
                case "temperature":
                    parameter = 'temperature';
                    over = data['max_temperature'];
                    under = data['min_temperature'];
                    break;
                case "brightness":
                    parameter = 'ambientLight';
                    over = data['max_brightness'];
                    under = data['min_brightness'];
            }

            var over1 = analyzeData(data['over1'], checkOver, parameter, over, 'Room 1');
            var over2 = analyzeData(data['over2'], checkOver, parameter, over, 'Room 2');
            var under1 = analyzeData(data['under1'], checkUnder, parameter, under, 'Room 1');
            var under2 = analyzeData(data['under2'], checkUnder, parameter, under, 'Room 2');

            var total = over1.concat(over2, under1, under2);
            total.sort(compareStartTime);
            drawLimitsTable(total);

            //console.log(analyzeData(data['over1'], checkOver, 'temperature', data['max_temperature']));
            //drawLimitsTable(data);
        }
    });

};

//analisi dei dati, identificazione intervalli di tempo in cui i valori superano una data soglia
var analyzeData = function(data, check, property, limit, room){
    var temp_list = [];
    var sum = 0;
    var result = [];

    console.log(data, property, limit);

    data.forEach(function(element){
        if(check(element[property], limit)){
            temp_list.push(element);
            sum += element[property];
        } else {
            if( temp_list.length > 1 ){
                var start = temp_list[0]['timestamp'];
                var finish = temp_list[temp_list.length-1]['timestamp'];
                var start_date = new Date(parseInt(start['$numberLong']));
                var finish_date = new Date(parseInt(finish['$numberLong']));
                //console.log(start, finish, start_date, finish_date)

                result.push({
                    room: room,
                    start: start_date,
                    interval :
                        "From " +
                        start_date.toDateString() + " - " +
                        start_date.getHours() + ":" +
                        start_date.getMinutes() + ":" +
                        start_date.getSeconds()+
                        " to " +
                        finish_date.toDateString() + " - " +
                        finish_date.getHours() + ":" +
                        finish_date.getMinutes() + ":" +
                        start_date.getSeconds(),

                    value :
                        (sum / temp_list.length).toFixed(2)
                });

                sum = 0;
                temp_list = [];
            }
        }
    });

    return result;
};

//check valori over soglia massima
var checkOver = function(value, max){
    return value >= max;
};

//check valori under soglia minima
var checkUnder = function(value, min){
    return value <= min;
};

//compare per l'ordinamento cronologico delle risposte
var compareStartTime = function(a, b){
    if (a.start.getTime() < b.start.getTime())
        return -1;
    if (a.start.getTime() > b.start.getTime())
        return 1;

    return 0;
};


//disegna la tabella con i dati ricavati
var drawLimitsTable = function(data){

    drawTableHeader();

    data.forEach(function(element){
        var room = element['room'];
        var value = element['value'];
        var interval = element['interval'];
        addRow(room, value, interval);
    });
};

//disegna l'header della tabella
var drawTableHeader = function(){

    $("body").append(
        "<table>\n" +
        "<tbody>" +
        "<tr>\n" +
        "<th id=\"room_th\">Room</th>\n" +
        "<th id=\"avg_th\">Average value</th>\n" +
        "<th id=\"time_th\">Time interval</th>\n" +
        "</tr>\n" +
        "</tbody>" +
        "</table>"
    );
};

//aggiunge dinamicamente una riga alla tabella
var addRow = function(room, value, interval){

    $('tbody:last-child').append(
        "<tr>" +
        "<td>" + room + "</td>" +
        "<td>" + value + "</td>" +
        "<td>" + interval + "</td>" +
        "</tr>"
    );
};

//modifica colore bottone selezionato
var changeColorSelectedButton = function(btn, btn_class){
    $(btn_class).css({
        "background-color": "white",
        "color": "black"
    });

    $(btn).css({
        "background-color": "teal",
        "color": "white"
    })
}
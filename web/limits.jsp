<%--
  Created by IntelliJ IDEA.
  User: martina
  Date: 05/02/18
  Time: 16.48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ADM2018 - Limits</title>
    <link rel="shortcut icon" href="img/beacon_icon.png">
    <script src="js/http_d3js.org_d3.v4.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/limits.js"></script>
    <link href="beacon_style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="buttons_div2">
        <button class="cmd" id="temp_log_btn" value="temperature">Temperature log</button>
        <button class="cmd" id="bright_log_btn" value="brightness">Brightness log</button>
        <button id="home_btn" value="home" onclick="window.location.href='home.jsp'">Homepage</button>
    </div>
</body>
</html>
